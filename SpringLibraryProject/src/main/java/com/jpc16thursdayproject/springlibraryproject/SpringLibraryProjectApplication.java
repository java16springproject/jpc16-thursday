package com.jpc16thursdayproject.springlibraryproject;

import com.jpc16thursdayproject.springlibraryproject.dbexample.MyDBConfigContext;
import com.jpc16thursdayproject.springlibraryproject.dbexample.dao.BookDAOBean;
import com.jpc16thursdayproject.springlibraryproject.dbexample.dao.BookDaoJDBC;
import com.jpc16thursdayproject.springlibraryproject.dbexample.db.DBConnection;
import com.jpc16thursdayproject.springlibraryproject.dbexample.model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.List;

@SpringBootApplication
public class SpringLibraryProjectApplication implements CommandLineRunner {

    @Value("${server.port}")
    private String serverPort;

    public static void main(String[] args) {
        SpringApplication.run(SpringLibraryProjectApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        BCryptPasswordEncoder encoder  = new BCryptPasswordEncoder();
        System.out.println(encoder.encode("admin"));

        System.out.println("Swagger path: http://localhost:" + serverPort + "/swagger-ui/index.html");
        System.out.println("Application path: http://localhost:" + serverPort + "/");

    }
}
