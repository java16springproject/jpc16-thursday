package com.jpc16thursdayproject.springlibraryproject.library.repository;

import com.jpc16thursdayproject.springlibraryproject.library.model.Book;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository
        extends GenericRepository<Book> {
}
