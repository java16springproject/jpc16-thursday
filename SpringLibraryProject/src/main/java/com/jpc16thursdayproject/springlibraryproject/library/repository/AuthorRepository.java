package com.jpc16thursdayproject.springlibraryproject.library.repository;

import com.jpc16thursdayproject.springlibraryproject.library.model.Author;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorRepository
        extends GenericRepository<Author> {

}
