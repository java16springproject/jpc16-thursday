package com.jpc16thursdayproject.springlibraryproject.library.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDate;
import java.util.List;

@ToString
@NoArgsConstructor
@Getter
@Setter
public class AuthorDTO extends GenericDTO {
    private String authorFIO;
    private LocalDate birthDate;
    private String description;
    List<Long> booksIds;

//    public AuthorDTO(Author author) {
//        this.id = author.getId();
//        this.createdWhen = author.getCreatedWhen();
//        List<Book> books = author.getBooks();
//        List<Long> bookIds = new ArrayList<>();
//        books.forEach(b -> bookIds.add(b.getId()));
//        this.booksIds = bookIds;
//    }
//
//    public static List<AuthorDTO> getAuthorDTOs(List<Author> authors) {
//        List<AuthorDTO> authorDTOS = new ArrayList<>();
//        for (Author author : authors) {
//            authorDTOS.add(new AuthorDTO(author));
//        }
//        return authorDTOS;
//    }


}
