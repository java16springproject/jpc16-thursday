package com.jpc16thursdayproject.springlibraryproject.library.service;


import com.jpc16thursdayproject.springlibraryproject.library.dto.BookRentInfoDTO;
import com.jpc16thursdayproject.springlibraryproject.library.mapper.BookRentInfoMapper;
import com.jpc16thursdayproject.springlibraryproject.library.model.BookRentInfo;
import com.jpc16thursdayproject.springlibraryproject.library.repository.BookRentInfoRepository;
import org.springframework.stereotype.Service;

@Service
public class BookRentInfoService
      extends GenericService<BookRentInfo, BookRentInfoDTO> {
    protected BookRentInfoService(BookRentInfoRepository bookRentInfoRepository,
                                  BookRentInfoMapper bookRentInfoMapper) {
        super(bookRentInfoRepository, bookRentInfoMapper);
    }
}
