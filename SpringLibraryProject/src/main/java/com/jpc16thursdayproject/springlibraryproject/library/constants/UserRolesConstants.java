package com.jpc16thursdayproject.springlibraryproject.library.constants;

public interface UserRolesConstants {
    String ADMIN = "ADMIN";
    String USER = "USER";
    String LIBRARIAN = "LIBRARIAN";
}
