package com.jpc16thursdayproject.springlibraryproject.library.repository;

import com.jpc16thursdayproject.springlibraryproject.library.model.BookRentInfo;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRentInfoRepository
      extends GenericRepository<BookRentInfo> {
}
